defmodule MfmParser.EncoderTest do
  use ExUnit.Case

  alias MfmParser.Encoder
  alias MfmParser.Node

  doctest MfmParser.Encoder

  describe "to_html" do
    test "it handles text" do
      input_tree = [%Node.Text{content: "chocolatine"}]

      expected = "chocolatine"

      assert Encoder.to_html(input_tree) == expected
    end

    test "it handles a node without attributes" do
      input = [%Node.MFM{name: "flip", attributes: %{}, content: []}]

      expected = "<span class=\"mfm-flip\"></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles a node with a non-value attribute" do
      input = [%Node.MFM{name: "font", attributes: [{"cursive"}], content: []}]

      expected = "<span class=\"mfm-font\" data-mfm-cursive></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles a node with one value attribute" do
      input = [%Node.MFM{name: "jelly", attributes: [{"speed", "2s"}], content: []}]

      expected = "<span class=\"mfm-jelly\" data-mfm-speed=\"2s\"></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles a node with multiple attributes" do
      input = [
        %Node.MFM{
          name: "spin",
          attributes: [{"alternate"}, {"speed", "0.5s"}],
          content: []
        }
      ]

      expected = "<span class=\"mfm-spin\" data-mfm-alternate data-mfm-speed=\"0.5s\"></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles multpile nodes on the same level" do
      input = [
        %Node.MFM{name: "twitch", attributes: [], content: []},
        %Node.Text{content: "chocolatine"},
        %Node.MFM{name: "blabla", attributes: [], content: []}
      ]

      expected = "<span class=\"mfm-twitch\"></span>chocolatine<span class=\"mfm-blabla\"></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles nesting" do
      input = [
        %Node.MFM{
          name: "twitch",
          attributes: [],
          content: [%Node.Text{content: "chocolatine"}]
        }
      ]

      expected = "<span class=\"mfm-twitch\">chocolatine</span>"

      assert Encoder.to_html(input) == expected
    end

    test "it handles complex nesting of nodes" do
      input = [
        %MfmParser.Node.Text{content: "It's not "},
        %MfmParser.Node.MFM{
          name: "twitch",
          attributes: [],
          content: [%MfmParser.Node.Text{content: "chocolatine"}]
        },
        %MfmParser.Node.Text{content: "\nit's "},
        %MfmParser.Node.MFM{
          name: "x4",
          attributes: [],
          content: [
            %MfmParser.Node.MFM{
              name: "spin",
              attributes: [{"alternate"}, {"speed", "0.2s"}],
              content: [%MfmParser.Node.Text{content: "pain"}]
            },
            %MfmParser.Node.Text{content: " "},
            %MfmParser.Node.MFM{
              name: "rainbow",
              attributes: [],
              content: [%MfmParser.Node.Text{content: "au"}]
            },
            %MfmParser.Node.Text{content: " "},
            %MfmParser.Node.MFM{
              name: "jump",
              attributes: [{"speed", "0.5s"}],
              content: [%MfmParser.Node.Text{content: "chocolat"}]
            }
          ]
        }
      ]

      expected =
        "It's not <span class=\"mfm-twitch\">chocolatine</span>\nit's <span class=\"mfm-x4\"><span class=\"mfm-spin\" data-mfm-alternate data-mfm-speed=\"0.2s\">pain</span> <span class=\"mfm-rainbow\">au</span> <span class=\"mfm-jump\" data-mfm-speed=\"0.5s\">chocolat</span></span>"

      assert Encoder.to_html(input) == expected
    end

    test "it should be able to go from mfm text input to html output" do
      input =
        "It's not $[twitch chocolatine]\nit's $[x4 $[spin.alternate,speed=0.2s pain] $[rainbow au] $[jump.speed=0.5s chocolat]]"

      expected =
        "It's not <span class=\"mfm-twitch\">chocolatine</span>\nit's <span class=\"mfm-x4\"><span class=\"mfm-spin\" data-mfm-alternate data-mfm-speed=\"0.2s\">pain</span> <span class=\"mfm-rainbow\">au</span> <span class=\"mfm-jump\" data-mfm-speed=\"0.5s\">chocolat</span></span>"

      assert input |> MfmParser.Parser.parse() |> Encoder.to_html() == expected
      assert input |> Encoder.to_html() == expected
    end

    test "it handles possible edge cases" do
      assert MfmParser.Parser.parse("") |> Encoder.to_html() == ""
      assert MfmParser.Parser.parse("]") |> Encoder.to_html() == "]"
      assert MfmParser.Parser.parse("[") |> Encoder.to_html() == "["
      assert MfmParser.Parser.parse("$") |> Encoder.to_html() == "$"
      assert MfmParser.Parser.parse("[1]") |> Encoder.to_html() == "[1]"

      assert MfmParser.Parser.parse("$$[spin beep]$") |> Encoder.to_html() ==
               "$<span class=\"mfm-spin\">beep</span>$"

      assert MfmParser.Parser.parse("$[spin boop]]") |> Encoder.to_html() ==
               "<span class=\"mfm-spin\">boop</span>]"

      # Behaviour of these is currently undefined
      # The important part is that they do not crash the whole thing
      MfmParser.Parser.parse("$[") |> Encoder.to_html()
      MfmParser.Parser.parse("$[]") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin ]") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin beep]$[") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin ") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin chocoretto") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin. ") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin. chocoretto") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin.x= ") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin.x= chocoretto") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin. chocoretto]") |> Encoder.to_html()
      MfmParser.Parser.parse("$[spin.x= chocoretto]") |> Encoder.to_html()
      MfmParser.Parser.parse("$[sp") |> Encoder.to_html()
    end
  end
end
