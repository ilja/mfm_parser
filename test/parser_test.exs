defmodule MfmParser.ParserTest do
  use ExUnit.Case
  alias MfmParser.Parser

  doctest MfmParser.Parser

  describe "single element input" do
    test "it can handle an empty string as input" do
      input = ""

      assert Parser.parse(input) == []
    end

    test "it can handle text as input" do
      input = "pain\nau\nchocolat"

      output = [%MfmParser.Node.Text{content: "pain\nau\nchocolat"}]

      assert Parser.parse(input) == output
    end

    test "it can handle an element without attributes" do
      input = "$[flip ]"

      output = [%MfmParser.Node.MFM{name: "flip", attributes: [], content: []}]

      assert Parser.parse(input) == output
    end

    test "it can handle an element with one non-value attribute" do
      input = "$[font.cursive ]"

      output = [%MfmParser.Node.MFM{name: "font", attributes: [{"cursive"}], content: []}]

      assert Parser.parse(input) == output
    end

    test "it can handle an element with one value attribute" do
      input = "$[jelly.speed=2s ]"

      output = [%MfmParser.Node.MFM{name: "jelly", attributes: [{"speed", "2s"}], content: []}]

      assert Parser.parse(input) == output
    end

    test "it can handle an element with multiple attributes" do
      input = "$[spin.alternate,speed=0.5s ]"

      output = [
        %MfmParser.Node.MFM{
          name: "spin",
          attributes: [{"alternate"}, {"speed", "0.5s"}],
          content: []
        }
      ]

      assert Parser.parse(input) == output
    end
  end

  describe "multiple element input" do
    test "it can handle multiple elements as input" do
      input = "$[twitch ]chocolatine$[blabla ]\n$[jump ]"

      output = [
        %MfmParser.Node.MFM{name: "twitch", attributes: [], content: []},
        %MfmParser.Node.Text{content: "chocolatine"},
        %MfmParser.Node.MFM{name: "blabla", attributes: [], content: []},
        %MfmParser.Node.Text{content: "\n"},
        %MfmParser.Node.MFM{name: "jump", attributes: [], content: []}
      ]

      assert Parser.parse(input) == output
    end

    test "it can handle nesting" do
      input = "$[twitch chocolatine]"

      output = [
        %MfmParser.Node.MFM{
          name: "twitch",
          attributes: [],
          content: [%MfmParser.Node.Text{content: "chocolatine"}]
        }
      ]

      assert Parser.parse(input) == output
    end

    test "it can handle multiple nestings" do
      input = "$[twitch $[spin chocolatine]]"

      output = [
        %MfmParser.Node.MFM{
          name: "twitch",
          attributes: [],
          content: [
            %MfmParser.Node.MFM{
              name: "spin",
              attributes: [],
              content: [%MfmParser.Node.Text{content: "chocolatine"}]
            }
          ]
        }
      ]

      assert Parser.parse(input) == output
    end

    test "it can handle a complex structure of multiple elements and nesting" do
      input =
        "It's not $[twitch chocolatine]\nit's $[x4 $[spin.alternate,speed=0.2s pain] $[rainbow au] $[jump.speed=0.5s chocolat]]"

      output = [
        %MfmParser.Node.Text{content: "It's not "},
        %MfmParser.Node.MFM{
          name: "twitch",
          attributes: [],
          content: [%MfmParser.Node.Text{content: "chocolatine"}]
        },
        %MfmParser.Node.Text{content: "\nit's "},
        %MfmParser.Node.MFM{
          name: "x4",
          attributes: [],
          content: [
            %MfmParser.Node.MFM{
              name: "spin",
              attributes: [{"alternate"}, {"speed", "0.2s"}],
              content: [%MfmParser.Node.Text{content: "pain"}]
            },
            %MfmParser.Node.Text{content: " "},
            %MfmParser.Node.MFM{
              name: "rainbow",
              attributes: [],
              content: [%MfmParser.Node.Text{content: "au"}]
            },
            %MfmParser.Node.Text{content: " "},
            %MfmParser.Node.MFM{
              name: "jump",
              attributes: [{"speed", "0.5s"}],
              content: [%MfmParser.Node.Text{content: "chocolat"}]
            }
          ]
        }
      ]

      assert Parser.parse(input) == output
    end
  end
end
