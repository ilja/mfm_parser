defmodule MfmParser.Node.Text do
  defstruct content: ""
end

defmodule MfmParser.Node.MFM do
  defstruct name: "", attributes: %{}, content: []
end
