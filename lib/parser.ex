defmodule MfmParser.Parser do
  alias MfmParser.Token
  alias MfmParser.Node
  alias MfmParser.Lexer

  @moduledoc """
  `MfmParser` is a [FEP-c16b](https://codeberg.org/fediverse/fep/src/branch/main/fep/c16b/fep-c16b.md) compatible parser for Misskey's [Markup language For Misskey](https://misskey-hub.net/en/docs/for-users/features/mfm/) MFM functions.

  It can parse a string representing text containing MFM functions and return a tree. There's also has an encoder who can turn a tree into HTML.

  It only parses the MFM specific tags of the form $[name.opts content].

  Other parts of MFM (html, Markdown and [KaTeX](https://katex.org/)) are out of scope for this project.

  ## Examples

      iex> MfmParser.Parser.parse("$[twitch.speed=0.5s 🍮]")
      [
        %MfmParser.Node.MFM{
          name: "twitch",
          attributes: [{"speed", "0.5s"}],
          content: [%MfmParser.Node.Text{content: "🍮"}]
        }
      ]
  """

  def parse(input, tree \\ [], is_open \\ false) do
    case Lexer.next(input) do
      {token, rest} ->
        case token do
          %Token.Text{} ->
            parse(
              rest,
              tree ++ [%Node.Text{content: token.content}],
              is_open
            )

          %Token.MFM.Open{} ->
            # Here we go deeper in the structure
            {children, rest} =
              case parse(rest, [], true) do
                {children, child_rest} -> {children, child_rest}
                # Here we capture an edge case where an unclosed tag makes us hit :eof
                # this causes the tree to be returned directly instead of part of a tuple
                children -> {children, ""}
              end

            # Here we went dept already, so now we are parsing the next Open token on the same level
            parse(
              rest,
              tree ++ [token |> get_mfm_node() |> Map.put(:content, children)],
              is_open
            )

          # We can either have a Close token who properly closes an Open token
          # Or we can have a stray Close token, while currently not processing an Open token
          # In the first case, we return what we have bc parsing of this Node is finished
          # In the second case, we add it as text
          %Token.MFM.Close{} ->
            if is_open do
              {tree, rest}
            else
              parse(
                rest,
                tree ++ [%Node.Text{content: token.content}]
              )
            end
        end

      :eof ->
        tree
    end
  end

  defp get_mfm_node(token) do
    {name, attributes} =
      case token.content
           |> String.trim()
           |> String.replace("$[", "")
           |> String.split(".", parts: 2) do
        [name] -> {name, []}
        [name, attributes_string] -> {name, build_attributes_list(attributes_string)}
      end

    %Node.MFM{name: name, attributes: attributes, content: []}
  end

  defp build_attributes_list(attributes_string) do
    attributes_string
    |> String.split(",")
    |> Enum.reduce([], fn attribute_string, acc ->
      attribute =
        case attribute_string |> String.split("=") do
          [name] -> {name}
          [name, value] -> {name, value}
        end

      acc ++ [attribute]
    end)
  end
end
