defmodule MfmParser.Encoder do
  @moduledoc """
  An encoder who can turn a String with MFM functions, or an MFM tree from `MfmParser.Parser.parse/1`, into FEP-c16b compliant HTML.

  It only works for the MFM specific tags of the form `$[name.attributes content]`. Other parts of MFM (e.g. html, Markdown and [KaTeX](https://katex.org/)) are out of scope for this project.

  ## Examples

      iex> [
      ...>   %MfmParser.Node.MFM{
      ...>     name: "twitch",
      ...>     content: [%MfmParser.Node.Text{content: "🍮"}],
      ...>     attributes: [{"speed", "5s"}]
      ...>   }
      ...> ]
      ...> |> MfmParser.Encoder.to_html()
      ~S[<span class="mfm-twitch" data-mfm-speed="5s">🍮</span>]

      iex> "$[twitch.speed=5s 🍮]" |> MfmParser.Encoder.to_html()
      ~S[<span class="mfm-twitch" data-mfm-speed="5s">🍮</span>]
  """

  def to_html([node | rest]) do
    node_html =
      case node do
        %MfmParser.Node.Text{content: content} ->
          content

        %MfmParser.Node.MFM{name: name, attributes: attributes, content: content} ->
          attributes_string =
            attributes
            |> Enum.reduce("", fn
              {name}, acc -> acc <> " data-mfm-#{name}"
              {name, value}, acc -> acc <> " data-mfm-#{name}=\"#{value}\""
            end)

          "<span class=\"mfm-#{name}\"#{attributes_string}>#{to_html(content)}</span>"
      end

    node_html <> to_html(rest)
  end

  def to_html([]) do
    ""
  end

  def to_html(mfm_string) when is_binary(mfm_string) do
    MfmParser.Parser.parse(mfm_string) |> to_html()
  end
end
